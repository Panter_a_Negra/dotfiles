#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x


dir="$HOME/.config/rofi/themes"
rofi_command="rofi -theme $dir/quicklinks.rasi"

# Error msg
msg() {
	rofi -theme "$HOME/.config/rofi/applets/styles/message.rasi" -e "$1"
}

# Links
gimp=""
inkscape=""
virtual=""
pavucontrol=""
htop=""
nvim=""

# Variable passed to rofi
options="$gimp\n$inkscape\n$virtual\n$pavucontrol\n$htop\n$nvim"

chosen="$(echo -e "$options" | $rofi_command -p "Open In  :  $app" -dmenu -selected-row 0)"
case $chosen in
    $gimp)
        gimp
        ;;
    $inkscape)
        inkscape
        ;;
    $virtual)
        virtualbox
        ;;
    $pavucontrol)
        pavucontrol
        ;;
    $htop)
        alacritty -e htop
        ;;
    $nvim)
        alacritty -e nvim
        ;;
esac

