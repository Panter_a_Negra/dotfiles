#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x


dir="$HOME/.config/rofi/themes"
rofi_command="rofi -theme $dir/quicklinks.rasi"

# Error msg
msg() {
	rofi -theme "$HOME/.config/rofi/applets/styles/message.rasi" -e "$1"
}

# Browser
app="firefox-bin"

# Links
deezer=""
unix=""
packages=""
proton=""
gmail=""
cidi=""

# Variable passed to rofi
options="$deezer\n$unix\n$packages\n$proton\n$gmail\n$cidi"

chosen="$(echo -e "$options" | $rofi_command -p "Open In  :  $app" -dmenu -selected-row 0)"
case $chosen in
    $deezer)
        $app https://www.deezer.com &
        ;;
    $unix)
        $app https://www.reddit.com/r/unixporn &
        ;;
    $packages)
        $app https://packages.gentoo.org/ &
        ;;
    $proton)
        $app https://www.protonmail.com &
        ;;
    $gmail)
        $app https://www.gmail.com &
        ;;
    $cidi)
        $app https://cidi.cba.gov.ar/portal-publico/ &
        ;;
esac

