#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x


dir="$HOME/.config/rofi/themes"
rofi_command="rofi -theme $dir/quicklinks.rasi"

# Error msg
msg() {
	rofi -theme "$HOME/.config/rofi/applets/styles/message.rasi" -e "$1"
}

# Browser
app="firefox-bin"

# Links
google=""
youtube=""
reddit=""
netflix=""
telegram=""
whatsapp=""

# Variable passed to rofi
options="$google\n$youtube\n$reddit\n$netflix\n$telegram\n$whatsapp"

chosen="$(echo -e "$options" | $rofi_command -p "Open In  :  $app" -dmenu -selected-row 0)"
case $chosen in
    $google)
        $app https://www.google.com &
        ;;
    $youtube)
        $app https://www.youtube.com &
        ;;
    $reddit)
        $app https://www.reddit.com &
        ;;
    $netflix)
        $app https://www.netflix.com &
        ;;
    $telegram)
        $app https:/web.telegram.org &
        ;;
    $whatsapp)
        $app https://web.whatsapp.com &
        ;;
esac

