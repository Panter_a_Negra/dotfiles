#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x


dir="$HOME/.config/rofi/themes"
rofi_command="rofi -theme $dir/quicklinks.rasi"

# Error msg
msg() {
	rofi -theme "$HOME/.config/rofi/applets/styles/message.rasi" -e "$1"
}

# Browser
app="thunar"

# Links
down=""
images=""
media=""
stuff=""
music=""
school=""

# Variable passed to rofi
options="$down\n$images\n$media\n$stuff\n$music\n$school"

chosen="$(echo -e "$options" | $rofi_command -p "Open In  :  $app" -dmenu -selected-row 0)"
case $chosen in
    $down)
        $app /home/pantera/Downloads
        ;;
    $images)
        $app /home/pantera/Images
        ;;
    $media)
        $app /home/pantera/Media
        ;;
    $stuff)
        $app /home/pantera/Stuff
        ;;
    $music)
        $app /home/pantera/Music
        ;;
    $school)
        $app /home/pantera/School
        ;;
esac

