"===============================================================================
"  ██╗      █████╗ ████████╗███████╗██╗  ██╗
"  ██║     ██╔══██╗╚══██╔══╝██╔════╝╚██╗██╔╝
"  ██║     ███████║   ██║   █████╗   ╚███╔╝ 
"  ██║     ██╔══██║   ██║   ██╔══╝   ██╔██╗ 
"  ███████╗██║  ██║   ██║   ███████╗██╔╝ ██╗
"  ╚══════╝╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝
"===============================================================================

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" VimTex
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
map <Leader>l <plug>(vimtex-compile) 

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Latex syntax
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Sections
inoremap ;s \section{}<Esc>F{a
inoremap ;S \section*{}<Esc>F{a
inoremap ;d \subsection{}<Esc>F{a
inoremap ;D \subsection*{}<Esc>F{a
inoremap ;f \subsection{}<Esc>F{a
inoremap ;F \subsection*{}<Esc>F{a

" Text types
inoremap ;b \textbf{}<Esc>F{a
inoremap ;i \textit{}<Esc>F{a
inoremap ;I \textbf{\textit{}}<Esc>F{a
inoremap ;B \textbf{\textit{}}<Esc>F{a

" Unordered list 
inoremap ;l \begin{itemize}<Enter>\item<Space><Enter>\item<Space><++><Enter>\item<Space><++><Enter>\item<Space><++><Enter>\item<Space><++><Enter>\end{itemize}<Esc>5ka

" Ordered list
inoremap ;L \begin{enumerate}<Enter>\item<Space><Enter>\item<Space><++><Enter>\item<Space><++><Enter>\item<Space><++><Enter>\item<Space><++><Enter>\end{enumerate}<Esc>5ka

" Images
inoremap ;g \begin{figure}[h]<Enter>\centering<Enter>\includegraphics[width=0.5\textwidth]{<++>}<Enter>\caption{<++>}<Enter>\end{figure}<Enter><ESC>i
