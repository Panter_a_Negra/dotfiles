"===============================================================================
"  ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗
"  ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║
"  ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║
"  ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║
"  ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║
"  ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝
"===============================================================================

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Basic configuration
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set path to current file
command! -bang -nargs=* Cd  cd %:p:h

set synmaxcol=800
set number            " show
set showcmd           " display incomplete commands
set hidden
set wildmenu          " visual autocomplete for command menu
set completeopt=menuone,noinsert,noselect
set shortmess+=atIc
set lazyredraw        " redraw only when we need to"
set mouse=a
set diffopt+=hiddenoff,algorithm:histogram
set cursorline
set number relativenumber

" make it obvious where 120 characters is
set textwidth=80
set colorcolumn=+1
set formatoptions+=w " for wraping long lines without broken words
set wrapmargin=0
set nowrap            " don't wrap long lines
set showmatch         " highlight matching brackets
set matchtime=5
set list

set novisualbell
set noerrorbells
set display=lastline
set laststatus=2
set showtabline=2
set noshowmode

set notimeout
set ttimeout
set ttimeoutlen=100
set updatetime=100

set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab

set encoding=utf-8
set smarttab
set autoindent        " indent
set linespace=0
set scrolloff=3
set sidescrolloff=5
set backspace=indent,eol,start
set formatoptions+=roj
set nrformats-=octal
set pastetoggle=<F2>  " switch paste mode
set dictionary+=/usr/share/dict/words-insane

set clipboard=unnamedplus " yank to and paste the selection without prepending "*
set history=10000       " sets how many lines of history VIM has to remember
set tabpagemax=50
set autoread          " when file was changed
set autowrite         " save file before switching a buffer
set ruler             " show the cursor position all the time
set nostartofline
set nohidden
set nojoinspaces
set sessionoptions-=options

set incsearch         " do incremental searching
set hlsearch          " highlight same words while searching with Shift + *
set ignorecase        " /the would find 'the' or 'The', add \C if you want 'the' only
set smartcase         " while /The would find only 'The' etc.

set foldenable
set foldmethod=marker
set foldnestmax=100

augroup AutoRead
    autocmd!
    autocmd FocusGained,BufEnter,CursorHold,CursorHoldI ?* if getcmdwintype() == '' | checktime | endif
    autocmd FileChangedShellPost * echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None
augroup END

augroup CustomFolding
    autocmd!
    autocmd BufWinEnter * let &foldlevel=max(add(map(range(1, line('$')), 'foldlevel(v:val)'), 10))  " with this, everything is unfolded at start
augroup End

function! NeatFoldText()
    let line = ' ' . substitute(getline(v:foldstart), '^\s*"\?\s*\|\s*"\?\s*{{' . '{\d*\s*', '', 'g') . ' '
    let lines_count = v:foldend - v:foldstart + 1
    let lines_count_text = '┤ ' . printf("%10s", lines_count . ' lines') . ' ├'
    let foldchar = matchstr(&fillchars, 'fold:\zs.')
    let foldtextstart = strpart('+ ' . repeat(foldchar, v:foldlevel*2) . line, 0, (winwidth(0)*2)/3)
    let foldtextend = lines_count_text . repeat(foldchar, 8)
    let foldtextlength = strlen(substitute(foldtextstart . foldtextend, '.', 'x', 'g')) + &foldcolumn
    return foldtextstart . repeat(foldchar, winwidth(0)-foldtextlength) . foldtextend
endfunction
set foldtext=NeatFoldText()

augroup SavePosition
    autocmd!
    autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | execute 'normal! g`"zvzz' | endif
augroup END

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.config/nvim/plugged')

"" File Browsing
Plug 'scrooloose/nerdtree'

" General
Plug 'mhinz/vim-startify'
Plug 'mbbill/undotree'
Plug 'haya14busa/incsearch.vim'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'lervag/vimtex'
Plug 'tweekmonster/startuptime.vim'
Plug 'vim-airline/vim-airline'

" Themes
"Plug 'jasolisdev/gruvbox' " fork of 'morhetz/gruvbox'
Plug 'ryanoasis/vim-devicons'
Plug 'arcticicestudio/nord-vim'
Plug 'joshdick/onedark.vim'

"" Language Specific
Plug 'ap/vim-css-color'
Plug 'moll/vim-node'
Plug 'mattn/emmet-vim'

call plug#end()


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Themes
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Theme and colors
set termguicolors
set background=dark
let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
"au ColorScheme * hi Normal ctermbg=none guibg=none
"au ColorScheme myspecialcolors hi Normal ctermbg=red guibg=red
colorscheme onedark

highlight SignColumn guibg=bg

"" Switch cursor according to mode
if &term !=? 'linux' || has('gui_running')
    let &t_SI="\<Esc>[6 q"
    let &t_SR="\<Esc>[4 q"
    let &t_EI="\<Esc>[2 q"
endif

 "" Startify
 " highlight StartifyBracket guifg=#ebdbb2 ctermfg=15 
 " highlight StartifyFooter  guifg=#ebdbb2 ctermfg=15 
 highlight StartifyHeader  guifg=#98c379 ctermfg=10 
 " highlight StartifyNumber  guifg=#ebdbb2 ctermfg=15 
 " highlight StartifyPath ctermfg=4 
 " highlight StartifyPath ctermfg=4 
 " highlight StartifySlash   guifg=#a89984 ctermfg=7 
 " highlight StartifySpecial guifg=#a89984 ctermfg=7 

 " Centers start screen vertically
function! s:vercent(head)
  let delt = ((&lines - len(a:head) - 15) / 2) - 1
  if delt<0
    return a:head
  endif
  let i = 0
  let head = a:head
  while i < delt
    let head = [''] + head
    let i = i + 1
  endwhile
  return head
endfunction

" Centers file entries horizontally. Hardcode offset, so long paths (over 50
" len) will break
function! s:horcent()
  let leng = 25 " delete this line if you figure out dynamic alignment"
  let delt = ((&columns - leng) / 2) - 1
  if delt<0
    return 0
  endif
  return delt
endfunction

let s:pad = <SID>horcent()

" Centers section header entries
function! s:bcent(str)
  let delt = ((&columns - len(a:str)) / 2) - 1
  if delt<0
    return a:str
  endif
  let i = 0
  let str = a:str
  while i < delt
    let str = " " . str
    let i = i + 1
  endwhile
  return str
endfunction

" opts
let g:startify_enable_special = 0
let g:startify_padding_left = s:pad


" formatted header/footer
 let s:headerr= [
            \'    _   __                _         ',
            \'   / | / /__  ____ _   __(_)___ ___ ',
            \'  /  |/ / _ \/ __ \ | / / / __ `__ \',
            \' / /|  /  __/ /_/ / |/ / / / / / / /',
            \'/_/ |_/\___/\____/|___/_/_/ /_/ /_/ ',
            \] 


let s:footer= ['-------------------------------','  Real Programmers use Neovim  ','-------------------------------']
let g:exten = ["vim.kronos - an endgame neovim config"]
let g:exten = [""]
let s:header = startify#center(s:headerr)
let g:startify_custom_header = <SID>vercent(s:header)
let g:startify_custom_footer = startify#center(s:footer) + startify#center(g:exten)

let g:startify_session_dir = '~/.config/nvim/session'
let g:startify_lists = [
          \ { 'type': 'bookmarks', 'header': [<SID>bcent("Acceso Rapido")]      },
           \ { 'type': 'sessions',  'header': [<SID>bcent("Sessions")]       },
          \ ]

let g:startify_bookmarks = [
            \ {'h': 'EnzoCalvi.tex' },
            \ {'j': 'index.html' },
            \ {'k': 'archivo.txt' },
            \ {'l': 'NewFile' },
            \ ]


if &term !=? 'linux' || has('gui_running')
    set listchars=tab:›\ ,extends:>,precedes:<,nbsp:˷,eol:⤶,trail:~
    set fillchars=vert:│,fold:─,diff:-
    augroup TrailingSpaces
        autocmd!
        autocmd InsertEnter * set listchars-=eol:⤶,trail:~
        autocmd InsertLeave * set listchars+=eol:⤶,trail:~
    augroup END
else
    set listchars=tab:>\ ,extends:>,precedes:<,nbsp:+,eol:$,trail:~
    set fillchars=vert:\|,fold:-,diff:-
    augroup TrailingSpaces
        autocmd!
        autocmd InsertEnter * set listchars-=eol:$,trail:~
        autocmd InsertLeave * set listchars+=eol:$,trail:~
    augroup END
endif

"" Airline
if !exists('g:airline_symbols')
    let g:airline_symbols={}
endif

""" Airline settings
let g:airline_theme='onedark'
let g:airline#extensions#whitespace#mixed_indent_algo=1
let g:airline_powerline_fonts=1
let g:airline_skip_empty_sections=0
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#buffer_idx_mode=1
let g:airline#extensions#tabline#tab_nr_type=1
let g:airline#extensions#tabline#show_tab_nr=0
let g:airline#extensions#tabline#show_close_button=0
let g:airline#extensions#tabline#exclude_preview=1
let g:airline#extensions#tabline#fnamecollapse=1
let g:airline#extensions#tabline#fnamemod=':~:.'
let g:airline#extensions#tabline#buffers_label='buffers'
let g:airline#extensions#tabline#tabs_label='tabs'
let g:airline#extensions#tabline#overflow_marker='…'
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_section_z='%3p%% %3l:%-2v'

"" Airline extensions
let g:airline#extensions#ale#error_symbol=''
let g:airline#extensions#ale#warning_symbol=''
let g:airline#extensions#ale#show_line_numbers=0
let g:airline#extensions#whitespace#show_message=1
let g:airline#extensions#hunks#enabled=0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins Settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Give more space for displaying messages.
set cmdheight=1

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

"" Undotree
nnoremap <silent> <leader>u :UndotreeToggle<CR>

"" Goyo + Limelight
let g:goyo_width=90
let g:goyo_height=90

function! s:goyo_enter()
    set nolist
    set noshowmode
    set noshowcmd
    set scrolloff=999
    set sidescrolloff=0
    Limelight
endfunction

function! s:goyo_leave()
    set list
    set noshowmode
    set showcmd
    set scrolloff=3
    set sidescrolloff=5
    Limelight!
endfunction

augroup Goyo
    autocmd!
    autocmd User GoyoEnter nested call <SID>goyo_enter()
    autocmd User GoyoLeave nested call <SID>goyo_leave()
augroup END

" Always show statusline
set laststatus=2

" Uncomment to prevent non-normal modes showing in powerline and below powerline.
set noshowmode

" Vimtex 
let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'zathura'

" Emmet
let g:user_emmet_mode='n'
let g:user_emmet_leader_key=','

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Import bindings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General mappings
source ~/.config/nvim/mapping.vim      

" Latex macros
autocmd FileType tex source ~/.config/nvim/latex.vim        
